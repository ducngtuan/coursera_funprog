package recfun
import common._

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int =
    if (c == 0) 1
    else
      if (c == r) 1
      else pascal(c, r-1) + pascal(c-1, r-1)

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {
    def _balance(chars: List[Char], diff: Int): Boolean = chars match {
      case Nil => diff == 0
      case '(' :: xs => _balance(xs, diff + 1)
      case ')' :: xs => if (diff > 0) _balance(xs, diff - 1) else false
      case _ :: xs => _balance(xs, diff)
    }

    _balance(chars, 0)
  }

  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int =
    if (money == 0) 1
    else
      if (coins.isEmpty) 0
      else
        if (money < coins.head) countChange(money, coins.tail)
        else
          countChange(money - coins.head, coins) + countChange(money, coins.tail)
}
